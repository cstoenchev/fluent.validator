﻿using System;
using System.Threading.Tasks;
using Fluent.Validator.Core;
using Fluent.Validator.Models;
using Fluent.Validator.Rules;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Nito.AsyncEx;

namespace Fluent.Validator.Executable
{
    class Program
    {
        static int Main(string[] args)
        {
            try
            {
                return AsyncContext.Run(() => MainAsync(args));
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex);
                return -1;
            }
        }

        static async Task<int> MainAsync(string[] args)
        {
            IServiceCollection serviceCollection = new ServiceCollection();

            serviceCollection.AddFluentValidator();

            IServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();

            var validationBuilder = serviceProvider.GetRequiredService<IValidationBuilder>();

            ValidationDataResult<int> successResult = await validationBuilder
                .Build()
                .AddRule<IdentifierShouldBePositive, long?>(1, "Id")
                .ValidateAsync()
                .OnSuccessWithValidationDataAsync(async () => { Console.WriteLine("This is called."); await Task.Delay(2000); return 5; });

            Console.WriteLine(successResult.ToActionResult(data => new CreatedResult(string.Empty, data)));

            ValidationResult errorResult = await validationBuilder
                .Build()
                .AddRule<IdentifierShouldBePositive, long?>(null, "OrganizationId")
                .AddRule<StringShoudNotBeEmpty, string>(string.Empty, "OrganizationName")
                .ValidateAsync();

            Console.WriteLine(errorResult.ToActionResult());

            Console.Read();

            return 0;
        }
    }
}