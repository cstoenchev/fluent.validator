﻿using System;
using Fluent.Validator.Models;
using Microsoft.AspNetCore.Mvc;

namespace Fluent.Validator.Executable
{
    public static class ValidationResultExtensions
    {
        public static IActionResult ToActionResult<T>(this ValidationDataResult<T> validationResult, Func<T, IActionResult> defaultActionFunc)
        {
            if (validationResult.IsValid)
            {
                return defaultActionFunc(validationResult.Data) ?? throw new ArgumentNullException(nameof(defaultActionFunc));
            }

            return validationResult.ToActionResult();
        }

        public static IActionResult ToActionResult(this ValidationResult validationResult, IActionResult DefaultAction = null)
        {
            var objectResult = new
            {
                Keys = validationResult.Errors.Keys,
                Errors = validationResult.Errors
            };

            switch (validationResult.ValidationError)
            {
                case ValidationErrorType.NoError:
                    {
                        return DefaultAction ?? new OkResult();
                    }

                case ValidationErrorType.InvalidParameter:
                    {
                        return new BadRequestObjectResult(objectResult);
                    }

                case ValidationErrorType.NotFound:
                    {
                        return new NotFoundObjectResult(objectResult);
                    }

                case ValidationErrorType.AlreadyExists:
                    {
                        return new ObjectResult(objectResult)
                        {
                            StatusCode = 409
                        };
                    }

                case ValidationErrorType.ReferenceRecordNotFound:
                    {
                        return new ObjectResult(objectResult)
                        {
                            StatusCode = 412
                        };
                    }

                default:
                    {
                        return new StatusCodeResult(500);
                    }
            }
        }
    }
}