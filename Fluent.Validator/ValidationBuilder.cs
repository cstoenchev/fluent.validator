﻿using System;
using Fluent.Validator.Core;
using Microsoft.Extensions.DependencyInjection;

namespace Fluent.Validator
{
    internal class ValidationBuilder : IValidationBuilder
    {
        #region Fields

        private readonly IServiceProvider _serviceProvider;

        #endregion

        #region Constructors

        public ValidationBuilder(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        #endregion

        #region Public Methods

        public IValidator Build()
        {
            return _serviceProvider.GetRequiredService<IValidator>();
        }

        #endregion
    }
}