﻿namespace Fluent.Validator.Models
{
    public class ValidationDataResult<TData> : ValidationResult
    {
        public TData Data { get; set; }
    }
}