﻿using System.Collections.Generic;

namespace Fluent.Validator.Models
{
    public class ValidationResult
    {
        public ValidationErrorType ValidationError { get; set; } = ValidationErrorType.NoError;

        public IDictionary<string, string> Errors { get; set; } = new Dictionary<string, string>();

        public bool IsValid => ValidationError == ValidationErrorType.NoError;
    }
}