﻿using System;

namespace Fluent.Validator.Models
{
    internal class ValidationRuleContext
    {
        public Type RuleType { get; set; }

        public object Parameter { get; set; }

        public string Key { get; set; }

        public string Message { get; set; }
    }
}
