﻿namespace Fluent.Validator.Models
{
    public enum ValidationErrorType
    {
        NoError = 0,

        InvalidParameter = 1,

        NotFound = 2,

        AlreadyExists = 3,

        ReferenceRecordNotFound = 4
    }
}
