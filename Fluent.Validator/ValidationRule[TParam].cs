﻿using System.Threading.Tasks;
using Fluent.Validator.Core;
using Fluent.Validator.Models;

namespace Fluent.Validator
{
    public abstract class ValidationRule<TParam> : IValidationRule
    {
        public abstract ValidationErrorType ValidationError { get; }

        public abstract string Message { get; }

        public Task<bool> ValidateAsync(object parameters)
            => ValidateAsync((TParam)parameters);

        public abstract Task<bool> ValidateAsync(TParam parameter);
    }
}