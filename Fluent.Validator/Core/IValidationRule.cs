﻿using System.Threading.Tasks;
using Fluent.Validator.Models;

namespace Fluent.Validator.Core
{
    public interface IValidationRule
    {
        ValidationErrorType ValidationError { get; }

        string Message { get; }

        Task<bool> ValidateAsync(object parameters);
    }
}
