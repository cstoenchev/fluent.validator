﻿using System;
using System.Threading.Tasks;
using Fluent.Validator.Models;

namespace Fluent.Validator.Core
{
    public interface IValidator
    {
        IValidator AddRule<TRule, TParam>(TParam parameters, string key, string message = "")
            where TRule : ValidationRule<TParam>;

        Task<ValidationResult> ValidateAsync();
    }
}