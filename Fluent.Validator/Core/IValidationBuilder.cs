﻿namespace Fluent.Validator.Core
{
    public interface IValidationBuilder
    {
        IValidator Build();
    }
}
