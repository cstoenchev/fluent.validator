﻿using System;
using System.Threading.Tasks;
using Fluent.Validator.Models;

namespace Fluent.Validator
{
    public static class ValidationResultExtensions
    {
        public static Task OnSuccessAsync(this Task<ValidationResult> validationResultTask, Func<Task> onSuccessFunc)
            => validationResultTask.OnSuccessWithValidationDataAsync(onSuccessFunc);

        public static async Task<ValidationResult> OnSuccessWithValidationDataAsync(this Task<ValidationResult> validationResultTask, Func<Task> onSuccessFunc)
        {
            ValidationResult validationResult = await validationResultTask;

            if (validationResult.IsValid)
            {
                await onSuccessFunc();
            }

            return validationResult;
        }

        public static async Task<TResult> OnSuccessAsync<TResult>(this Task<ValidationResult> validationResultTask, Func<Task<TResult>> onSuccessFunc)
            => (await validationResultTask.OnSuccessWithValidationDataAsync(onSuccessFunc)).Data;

        public static async Task<ValidationDataResult<TResult>> OnSuccessWithValidationDataAsync<TResult>(
            this Task<ValidationResult> validationResultTask, 
            Func<Task<TResult>> onSuccessFunc)
        {
            ValidationResult validationResult = await validationResultTask;

            TResult result = validationResult.IsValid
                ? await onSuccessFunc()
                : default(TResult);

            return new ValidationDataResult<TResult>
            {
                ValidationError = validationResult.ValidationError,
                Errors = validationResult.Errors,
                Data = result
            };
        }
    }
}