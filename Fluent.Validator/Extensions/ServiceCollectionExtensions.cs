﻿using Fluent.Validator.Core;
using Fluent.Validator.Rules;
using Microsoft.Extensions.DependencyInjection;

namespace Fluent.Validator
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddFluentValidator(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IValidationBuilder, ValidationBuilder>();
            serviceCollection.AddTransient<IValidator, Validator>();

            // TODO Add all rules with assembly
            serviceCollection.AddTransient<IdentifierShouldBePositive>();
            serviceCollection.AddTransient<StringShoudNotBeEmpty>();

            return serviceCollection;
        }
    }
}