﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Fluent.Validator.Core;
using Fluent.Validator.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Fluent.Validator
{
    internal class Validator : IValidator
    {
        #region Fields

        private readonly IServiceProvider _serviceProvider;

        private ICollection<ValidationRuleContext> _rules = new List<ValidationRuleContext>();

        #endregion

        #region Constructors

        public Validator(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        #endregion

        #region Public Methods

        public IValidator AddRule<TRule, TParam>(TParam parameters, string key, string message)
            where TRule : ValidationRule<TParam>
        {
            var context = new ValidationRuleContext
            {
                RuleType = typeof(TRule),
                Parameter = parameters,
                Key = key,
                Message = message
            };

            _rules.Add(context);

            return this;
        }

        public async Task<ValidationResult> ValidateAsync()
        {
            var result = new ValidationResult();

            foreach (var context in _rules)
            {
                var ruleInstance = _serviceProvider.GetRequiredService(context.RuleType) as IValidationRule;

                if (result.IsValid || ruleInstance.ValidationError == result.ValidationError)
                {
                    bool isValid = await ruleInstance.ValidateAsync(context.Parameter);

                    if (!isValid)
                    {
                        string message = string.IsNullOrWhiteSpace(context.Message)
                            ? ruleInstance.Message
                            : context.Message;

                        result.ValidationError = ruleInstance.ValidationError;

                        if (!result.Errors.ContainsKey(context.Key))
                        {
                            result.Errors.Add(context.Key, ruleInstance.Message);
                        }
                    }
                }
            }

            return result;
        }

        #endregion
    }
}