﻿using System.Threading.Tasks;
using Fluent.Validator.Models;

namespace Fluent.Validator.Rules
{
    public class StringShoudNotBeEmpty : ValidationRule<string>
    {
        public override ValidationErrorType ValidationError => ValidationErrorType.InvalidParameter;

        public override string Message => "String should have value.";

        public override Task<bool> ValidateAsync(string parameter)
            => Task.FromResult(!string.IsNullOrWhiteSpace(parameter));
    }
}
