﻿using System.Threading.Tasks;
using Fluent.Validator.Models;

namespace Fluent.Validator.Rules
{
    public class IdentifierShouldBePositive : ValidationRule<long?>
    {
        public override ValidationErrorType ValidationError => ValidationErrorType.InvalidParameter;

        public override string Message => "Identifier should be positive";

        public override Task<bool> ValidateAsync(long? parameter)
            => Task.FromResult((parameter ?? 0) > 0);
    }
}